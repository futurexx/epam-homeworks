def partial(func, *fargs, **fkwargs):
    def nfunc(*args, **kwargs):
        fkwargs.update(kwargs)
        return func(*(fargs + args), **fkwargs)
    nfunc.__name__ = "partical_" + func.__name__
    nfunc.__doc__ =("A partial implementation of {fname} "
                    "with pre-applied arguements being:\n"
                    "{fargs}\n"
                    "{fkwargs}").format(
                            fname=func.__name__,
                            fargs=fargs,
                            fkwargs=fkwargs)

    return nfunc

