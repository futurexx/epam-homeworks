import string

numbers = string.digits


def to_digit(string, num=0, mult=1):
    if (not string and mult != 1): 
        return num / 2 if (num % 2 == 0) else num * 3 + 1
    if string[-1] in numbers: 
        return to_digit(string[:-1],
                        num + numbers.index(string[-1]) * mult,
                        mult * 10)
    else: return


if __name__ == "__main__":
    while True:
        input_string = input()
        num = to_digit(input_string)

        if num: 
            print(num)
        elif input_string == 'cancel':
            break
        else: 
            print("Не удалось преобразовать введенный текст в число")
