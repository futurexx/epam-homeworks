import math

def mysqrt(num, eps=0.01):
    def newton(point):
        next_point = 0.5 * (point + num / point)
        if point - next_point < eps:
            return round(point, int(abs(math.log10(eps))))
        else: 
            return newton(next_point)
    return newton(1)

