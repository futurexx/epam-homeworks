def gcd(a, b):
    if b:
        return gcd(b, a % b)
    return a

def line(a, b):
    return gcd(abs(a[0] - b[0]), abs(a[1] - b[1]))

    
def geron(x, y, z):
    a = ((y[0] - x[0]) ** 2 + (y[1] - x[1]) ** 2) ** .5
    b = ((z[0] - x[0]) ** 2 + (z[1] - x[1]) ** 2) ** .5
    c = ((z[0] - y[0]) ** 2 + (z[1] - y[1]) ** 2) ** .5  
    p = (a + b + c) / 2

    return (p * (p - a) * (p - b) * (p - c)) ** .5


def count_points(a, b, c):
    border = line(a, b) + line(b, c) + line(c, a)
    count = geron(a, b, c) - border / 2 + 1
    return round(count + border)


if __name__ == "__main__":
    count_points((0,0),(1,0),(0,1))
    count_points((0,0),(2,0),(0,2))
    count_points((0,3),(1,1),(2,3))

