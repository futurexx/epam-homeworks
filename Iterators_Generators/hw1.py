class GraphIterator:
    def __init__(self, graph, start_point):
        self.graph = graph
        self.current_point= start_point
        self.checked_points = []
        self.stack = [start_point]

    def has_next(self):
        if self.stack:
            self.current_point = self.stack.pop(0)
            if self.current_point not in self.checked_points:
                self.checked_points.append(self.current_point)            
            for point in self.graph[self.current_point]:
                if point not in self.checked_points:
                    self.checked_points.append(point)
                    self.stack += self.graph[point]
            return True
        else: 
            return False

    def __next__(self):
        if self.has_next():
            return self.current_point    
        else:
            raise StopIteration


class Graph:    
    def __init__(self, start_point, E):
        self.E = E
        self.start_point = start_point
 
    def __iter__(self):
        return GraphIterator(self.E, self.start_point)
