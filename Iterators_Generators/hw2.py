import tempfile

def merge_files(fname1, fname2):
    with tempfile.NamedTemporaryFile(mode="w", delete=False) as res:
        with open(fname1) as f1, open(fname2) as f2:
            line1 = f1.readline()
            line2 = f2.readline()
            while line1 or line2:
                if not line1: 
                    res.write(line2)
                    line2 = f2.readline()
                elif not line2:
                    res.write(line1)
                    line1 = f1.readline()
                elif int(line1) > int(line2):
                    res.write(line2)
                    line2 = f2.readline()
                else:
                    res.write(line1)
                    line1 = f1.readline()
        
        return res.name
