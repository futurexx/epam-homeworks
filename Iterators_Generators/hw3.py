import os

def hardlink_check(dir_path):
    if os.path.isdir(dir_path):
        dir_list =map(lambda dl: os.path.join(dir_path, dl), os.listdir(dir_path))
        files_path = filter(lambda fp: os.path.isfile(fp), dir_list)
        files_ino = map(lambda fi: os.stat(fi).st_ino, files_path)
        uniq_ino = set()
        for ino in files_ino:
            if ino in uniq_ino: return True
            uniq_ino.add(ino)
    
    return False
