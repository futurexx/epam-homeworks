import string
from functools import reduce

operators = {
        '+': 1, 
        '-': 1,
        '*': 2,
        '/': 2,
            }

operands = string.ascii_lowercase

brackets = '()'

def exp_valid(exp):
    exp = exp.replace(' ', '')
    alphabet = (operands
                     + brackets 
                     + reduce((lambda x, y: x + y), operators.keys()))
    br_count = 0
    for token in exp:
        if token not in alphabet: return False 

        if token is brackets[0]: br_count += 1

        if token is brackets[1]: br_count -= 1

    if br_count != 0: return False

    return True


def to_postfix(infix):
    stack = []
    postfix = ''
    for token in infix:
        if token in operands: yield token
            
        elif token is brackets[0]: stack.append(token)

        elif token is brackets[1]:
            while stack:
                s = stack.pop()
                if s == brackets[0]: break
                yield s
    
        elif token in operators:
            while (stack and
                   stack[-1] is not brackets[0] and
                   operators[token] <= operators[stack[-1]]):
                yield stack.pop()
            stack.append(token)
            
    while stack: yield stack.pop()


def to_infix(postfix):
    stack = []
    for token in postfix:
        if token in operands:
            stack.append(token)
        else:
            t1,  t2 = stack.pop(), stack.pop()
            
            p = (lambda x: max(
                        [operators[s] if s in operators else 0 for s in x]
                        )
                    )
            if (p(t1) < operators[token] and p(t1) != 0): t1 = '(' + t1 + ')'
            if (p(t2) < operators[token] and p(t2) != 0): t2 = '(' + t2 + ')'
            stack.append(t2 + token + t1)

    return stack.pop()


def brackets_trim(exp):
    if exp_valid(exp):
        return to_infix(to_postfix(exp))

