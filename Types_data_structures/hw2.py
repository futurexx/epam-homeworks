def max_word_freq(string):
    d = {}
    for word in string.split(): d[word] = d.get(word, 0) + 1
    max_freq = max([d[key] for key in d.keys()])
    result = []
    for key in d.keys():
        if d[key] == max_freq: result.append((key, str(d[key])))
    return result


if __name__ == "__main__":
    while(True):
        string = input().lower()
        if string == "cancel":
            print("Bye!")
            break
        else:
            for point in max_word_freq(string):
                print(" - ".join(point))

