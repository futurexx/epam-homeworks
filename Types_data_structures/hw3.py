import re


while(True):
    string = input().lower()
    if string == "cancel":
        print("Bye!")
        break
    else:
        print(sum([int(number) for number in re.findall(r"\-?[0-9]+", string)]))
