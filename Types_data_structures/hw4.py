def next_min_number(string):
    numbers = [int(x) for x in string.split()]
    next_numbers = [i + 1 for i in numbers if i + 1 not in numbers]
    return min(next_numbers)


if __name__ == "__main__":
    while(True):
        string = input()
        if string == "cancel":
            print("Bye!")
            break
        else:
            print(next_min_number(string))
    
