import string

# operators priority
operators = {
        '+':  3,
        '-':  3,
        '*':  2,
        '/':  2,
        '^':  1,
            }
# list of correct operands 
operands = string.ascii_lowercase + string.digits


class Calculator:
    """
       Calculate exprations
    """
    def __init__(self, opcodes, operators=None):
        self.opcodes = opcodes
        self.operators = operators if operators is not None else []
    
    def validate(self,exp):
        """
            Try to built computation tree using two stack
            one for operators and other for operands 
        """
        exp = ['('] + exp + [')'] # bracket input expression
        ptoken = exp[0]           # previous token 
        b_balance = 0            # brackets balance
        o_balance = 0             # operands/operators balance
        for token in exp:
            # first a all checking division by zero
            if ptoken is '/' and token is '0':
                return False

            # checking  brackets balance
            if token in '()':
                b_balance += 1 if token is '(' else -1
            # cheacking operands/operators balance
            elif token in operators:
                # exclude unary minus
                if (ptoken is not '(' and
                    token is not '-'): o_balance -= 1
            elif token in operands: 
                o_balance += 1
            # also have an incorreсt token
            else:
                return False
        # final condition
        if b_balance != 0 or o_balance != 1:
            return False
        return True
    
    def __str__(self):
        """
            Converting expression from infix form
            to postfix form
        """
        stack = []   # stack of operators
        postfix = '' # result

        for token in self.opcodes:
            if token in operands: postfix += token
                
            elif token is '(': stack.append(token)
            # take away from stack all operations
            # until "(:
            elif token is ')':
                while stack:
                    s = stack.pop()
                    if s == '(': break
                    postfix += s
            # take away from stack operations with
            # with less priority current token
            elif token in operators:
                while (stack and
                       stack[-1] is not '(' and
                       operators[token] >= operators[stack[-1]]):
                    postfix += stack.pop()
                stack.append(token)
        # take away all from stack 
        while stack: postfix += stack.pop()
        return postfix

# Calculator tests
if __name__ == "__main__":
    validate_check_list = [
	('a+2', True),
	('a-(-2)', True),
	('a+2-', False),
	('a+(2+(3+5)', False),
	('a^2', True),
	('a^(-2)', True),
	('-a-2', True),
	('6/0', False),
	('a/(b-b)', True),
    ]

    str_check_list = [
	("a", "a"),
	("-a", "a-"),
	("(a*(b/c)+((d-f)/k))", "abc/*df-k/+"),
	("(a)", "a"),
	("a*(b+c)", "abc+*"),
	("(a*(b/c)+((d-f)/k))*(h*(g-r))", "abc/*df-k/+hgr-**"),
	("(x*y)/(j*z)+g", "xy*jz*/g+"),
	("a-(b+c)", "abc+-"),
	("a/(b+c)", "abc+/"),
	("a^(b+c)", "abc+^"),
    ]

    #validate tests
    for case, exp in validate_check_list:
        tokens = list(case)
        calc = Calculator(tokens).validate()

        if calc != exp:
            print(f'"{case}": actual "{calc}", expected {exp}')

    # __str__ tests
    for case, exp in str_check_list:
        tokens = list(case)
        calc = Calculator(tokens)

        if str(calc) != exp:
            print(f'"{case}": actual "{calc}", expected {exp}')

